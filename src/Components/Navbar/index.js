import React, { useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import {Link} from "react-router-dom";
import logo from './img/logo.png';
import logout from '../Logout/logout';

function ScpNavbar(args) {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar {...args}>
                <NavbarBrand href="/">
                <img
                    alt="logo"
                    src={logo}
                    style={{
                        height: 50,
                        width: 50,
                        borderRadius: "50%",
                        marginRight: 10,
                    }}
                />
                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className={"container-fluid me-auto"} navbar>
                        <NavItem>
                            <NavLink tag={Link} to="/pessoas">Pessoas</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to="/presencas">Presenças</NavLink>
                        </NavItem>
                        <Nav className="me-auto"></Nav>
                        <NavItem className="ml-auto">
                            <NavLink tag={Link} onClick={logout} >Logout</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </div>
    );
}

export default ScpNavbar;