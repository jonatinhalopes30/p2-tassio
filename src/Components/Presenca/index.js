import React from 'react';

import Tabela from './Tabela';

function PresencaBox() {

    return (
        <div className="container-fluid align-center">
            <div className="row">
                <div className="offset-1 col-10 offset-md-3 col-md-6 my-4 ">
                    <h2 className="font-weight-bold text-center">Presenças</h2>
                    <Tabela />
                </div>
            </div>
        </div>
    );
}
export default PresencaBox;