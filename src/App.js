import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import PessoaBox from "./Components/Pessoa";
import Login from "./Components/Login";
import useToken from './Components/Login/useToken';
import PresencaBox from "./Components/Presenca";
import ScpNavbar from "./Components/Navbar";

function App() {

  const { token, setToken } = useToken();

  if(!token) {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<Login setToken={setToken} />} />
        </Routes>
      </BrowserRouter>
    )
  }

  return (
    <BrowserRouter>
      <ScpNavbar color={"dark"} dark={true} expand={"sm"} />
      <Routes>
        <Route path="/" >
          <Route path="pessoas" element={<PessoaBox />} />
          <Route path="presencas" element={<PresencaBox />} />
          <Route path="login" element={<Login setToken={setToken} />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
