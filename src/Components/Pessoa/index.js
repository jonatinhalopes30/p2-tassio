import React, { useState } from 'react';

import Formulario from './Formulario';
import Tabela from './Tabela';

function PessoaBox() {

    const [pessoaFormData, setPessoaFormData] = useState({})
    const [reload, setReload] = useState(true)

    return (
        <div>
            <div className="row m-3">
                <div className="col-md-12 p-4 d-flex flex-column position-static">
                    <Tabela setPessoaFormData={setPessoaFormData} reload={reload} setReload={setReload}/>
                </div>
            </div>
            <div className='row m-3 justify-content-center'>
                <div className="col-md-8 p-4 d-flex flex-column bg-lights rounded"> 
                    <h2 className="font-weight-bold text-center">Cadastro</h2>
                    <hr className="my-4" />
                    <Formulario pessoaFormData={pessoaFormData} setPessoaFormData={setPessoaFormData} setReload={setReload}/>
                </div>
            </div>
        </div>
    );
}
export default PessoaBox;