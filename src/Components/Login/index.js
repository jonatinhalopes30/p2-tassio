import React, { useState } from 'react';
import logo from './img/logo.png';
import { useNavigate } from "react-router-dom";

import './Login.css';

async function loginUser(credentials) {
 return fetch('http://localhost:8080/login', {
   method: 'POST',
   headers: {
     'Content-Type': 'application/json'
   },
   body: JSON.stringify(credentials)
 })
   .then(data => data.json())
}

export default function Login({ setToken }) {

  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const navigate = useNavigate();

  const handleSubmit = async e => {
    e.preventDefault();
    const token = await loginUser({
      username,
      password
    });
    setToken(token);
    navigate('/pessoas')
  }

  return(
    <div className="container-login">
      <div className="login">
        <img src={logo} className={"logo-img"} alt="Logo UV"></img>
        <form className="form-login" onSubmit={handleSubmit}>
          <label>
            <p>Username</p>
            <input type="text" onChange={e => setUserName(e.target.value)} />
          </label>
          <label>
            <p>Password</p>
            <input type="password" onChange={e => setPassword(e.target.value)} />
          </label>
          <div className={"btn-login-container"}>
            <button className={"btn-login"} type="submit">Log In</button>
          </div>
        </form>
      </div>
    </div>
  )
}
