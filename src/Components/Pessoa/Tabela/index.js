import React, { useEffect, useState, useCallback } from 'react';
import { Table, Button } from 'reactstrap';
import defaultPhoto from './img/defaultPhoto.png';
import './tabela.css'

export default function Tabela({setPessoaFormData, reload, setReload}) {

    const baseURL = "http://localhost:8080/pessoa";

    const [users, setUsers] = useState([]);

    const token = localStorage.getItem('token')

    const load = useCallback(async () => {
        if (reload === true) {
            await fetch(`${baseURL}`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then(data => {
                    return data.json();
                })
                .then(data => {
                    setUsers(data);
                })
                .catch(err => {
                    console.log(err);
                });
            setReload(false)
        }
    }, [token, reload, setReload])

    useEffect(() => {
        load().catch(console.error);
    }, [load]);

    async function updateById(pessoa) {
        setPessoaFormData(pessoa)
    }

    async function deleteById(id) {
        if (id) {
            await fetch(`${baseURL}/${id}`, {
                method: "delete",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then(res => console.log(res))
            setReload(true)
        }
    }

    return (
        <Table className="table-bordered text-center">
            <thead className="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Foto</th>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Matrícula</th>
                    <th>Nascimento</th>
                    <th>Genero</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                {users.map((user, index) => (
                    <tr key={index} className="align-middle">
                        <td>{user.idPessoa}</td>
                        <td>
                            <img src={user.foto || defaultPhoto} className={"foto-pessoa"} alt="foto" />
                        </td>
                        <td>{user.nome}</td>
                        <td>{user.cpf}</td>
                        <td>{user.matricula}</td>
                        <td>{user.nascimento}</td>
                        <td>{user.genero}</td>
                        <td>
                            <Button color="primary" size="sm" onClick={() => updateById(user)}>Editar</Button>
                            {' '}
                            <Button color="danger" size="sm" onClick={() => deleteById(user.idPessoa)}>Deletar</Button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
}