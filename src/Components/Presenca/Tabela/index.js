import React, { useEffect, useState, useCallback } from 'react';
import {Table, Button, Label, Input, FormGroup} from 'reactstrap';

export default function Tabela() {

    const baseURL = "http://localhost:8080/presenca";

    const [reload, setReload] = useState(false)
    const [data, setData] = useState('')
    const [pessoas, setPessoas] = useState([]);
    const [presencas, setPresencas] = useState([]);
    const [presencasFormset, setPresencasFormset] = useState([]);

    const token = localStorage.getItem('token')

    const loadPessoa = useCallback(async () => {
        await fetch("http://localhost:8080/pessoa", {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then(data => {
                return data.json();
            })
            .then(data => {
                setPessoas(data);
            })
            .catch(err => {
                console.log(err);
            });
    }, [token])

    const loadPresenca = useCallback(async () => {
        if (data !== '') {
            await fetch(`${baseURL}?data=${data}`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then(data => {
                    return data.json();
                })
                .then(data => {
                    setPresencas(data);
                })
                .catch(err => {
                    console.log(err);
                });
            setReload(true)
        }
    }, [token, data])

    useEffect(() => {
        loadPresenca().catch(console.error);
        loadPessoa().catch(console.error);
    }, [loadPessoa, loadPresenca, data]);

    useEffect(() => {
        function initPresencaFormset() {
            let presencasFormsetTmp = []
            pessoas.forEach(function (pessoa, index, array) {
                let presencaTmp = presencas.find(p => p.pessoa?.idPessoa === pessoa.idPessoa) || {}
                presencasFormsetTmp[index] = {
                    "idPresenca": presencaTmp?.idPresenca,
                    "situacao": presencaTmp?.situacao,
                    "data": data,
                    "pessoa": pessoa || {}
                }
            })
            setPresencasFormset(presencasFormsetTmp)
        }
        initPresencaFormset();
        setReload(false)
    }, [reload, data, pessoas, presencas]);

    async function deleteById(id) {
        if (id) {
            await fetch(`${baseURL}/${id}`, {
                method: "delete",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            })
                .then(res => console.log(res))
            setReload(true)
        }
    }

    async function postData() {
        let presencasFormsetTmp = [...presencasFormset]
        presencasFormset.forEach(await async function (presencasForm, index) {
            if (presencasForm.idPresenca === undefined && presencasForm.situacao === undefined) {
                return;
            }
            if (presencasForm.idPresenca !== undefined && presencasForm.situacao === '') {
                await deleteById(presencasForm.idPresenca)
                return {
                    "pessoa": presencasForm?.pessoa || {},
                    "idPresenca": undefined,
                    "situacao": undefined
                }
            }
            try {
                const data = await fetch(`${baseURL}`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify(presencasForm),
                }).then(data => {
                    return data.json();
                }).catch(err => {
                    console.log(err);
                });
                presencasFormsetTmp[index].idPresenca = data?.idPresenca;
            } catch (err) {
                console.log(err);
            }
        })
        setPresencasFormset(presencasFormsetTmp);
    }

    function setPresencasFormSituacao(index, value) {
        let presensecasFormsetTmp = [...presencasFormset]
        presensecasFormsetTmp[index].situacao = value
        setPresencasFormset(presensecasFormsetTmp)
        return presensecasFormsetTmp[index]
    }

    return (
        <div>
            <FormGroup>
                <Label for="nome">Data:</Label>
                <Input id="nome" value={data || ''} onChange={(e) => setData(e.target.value)} required type='date' placeholder='Informe a data da presença' max={new Date().toISOString().split('T')[0]} />
            </FormGroup>
            <Table className="table-bordered text-center">
                <thead className="thead-dark">
                    <tr>
                        <th>Pessoa</th>
                        <th>Situação</th>
                    </tr>
                </thead>
                <tbody>
                    {data !== '' && presencasFormset?.map((presencaForm, index) => (
                        <tr key={index}>
                            <td hidden>
                                <Input type="hidden" value={presencaForm.idPresenca || ''} />
                            </td>
                            <td>
                                {presencaForm.pessoa?.nome || ''}
                            </td>
                            <td>
                                <Input type="select" value={presencasFormset[index]?.situacao !== undefined ? presencasFormset[index].situacao.toString() : ''}
                                       onChange={(e) => {setPresencasFormSituacao(index, e.target.value)}}>
                                    <option value={''} hidden selected>- - - -</option>
                                    <option value={'true'}>Presente</option>
                                    <option value={'false'}>Ausente</option>
                                </Input>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            <div className="text-center">
                <Button onClick={postData} color="primary">Salvar</Button>
            </div>
        </div>
    );
}