import { Button, Form, FormGroup, Input, Label, FormText } from 'reactstrap';

export default function Formulario({pessoaFormData, setPessoaFormData, setReload}) {

    const baseURL = "http://localhost:8080/pessoa";

    const token = localStorage.getItem('token')

    async function postData() {
        try {
            await fetch(`${baseURL}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(pessoaFormData),
            });
            if (pessoaFormData.idPessoa === undefined) {
                setPessoaFormData({})
            }
        } catch (err) {
            console.log(err);
        }
        setReload(true)
    }

    async function clearForm() {
        setPessoaFormData({})
        setReload(true)
    }

    function setPessoaFieldValue(field, value) {
        let pessoaTmp = {...pessoaFormData}
        pessoaTmp[field] = value
        setPessoaFormData(pessoaTmp)
    }

    const getBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file)
            fileReader.onload = () => {
                resolve(fileReader.result);
            }
            fileReader.onerror = (error) => {
                reject(error);
            }
        })
    }

    const handleFotoChange = async (e) => {
        let file = e.target.files[0];
        let base64 = await getBase64(file)
        setPessoaFieldValue('foto', base64)
    }

    return (
        <Form>
            <Input id="pessoa_id" value={pessoaFormData.idPessoa || ''} type='hidden' />
            <FormGroup>
                <div className="form-row">
                    <div className="col-12">
                        <FormGroup>
                            <Label for="nome">Nome:</Label>
                            <Input id="nome" value={pessoaFormData.nome || ''} onChange={(e) => setPessoaFieldValue('nome', e.target.value, )} type='text' placeholder='Informe o nome do usuário' />
                        </FormGroup>
                    </div>
                </div>
            </FormGroup>
            <FormGroup>
                <div className="form-row">
                    <div className="col-12">
                        <FormGroup>
                            <Label for="cpf">CPF:</Label>
                            <Input id="cpf" type="text" value={pessoaFormData.cpf || ''} onChange={(e) => setPessoaFieldValue('cpf', e.target.value)} pattern="\d{3}\.\d{3}\.\d{3}-\d{2}"
                                title="Digite um CPF no formato: xxx.xxx.xxx-xx" />
                        </FormGroup>
                    </div>
                    <div className="col-12">
                        <FormGroup>
                            <Label for="matricula">Matrícula:</Label>
                            <Input id="matricula" type="number" value={pessoaFormData.matricula || ''} onChange={(e) => setPessoaFieldValue('matricula', e.target.value)} />
                        </FormGroup>
                    </div>
                    <div className="col-12">
                        <FormGroup>
                            <Label for="nascimento">Nascimento:</Label>
                            <Input id="nascimento" type="date" value={pessoaFormData.nascimento || ''} onChange={(e) => setPessoaFieldValue('nascimento', e.target.value)} />
                        </FormGroup>
                    </div>
                    <div className="col-12">
                        <FormGroup>
                            <Label for="genero">Gênero:</Label>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="genero" checked={pessoaFormData.genero === 'M'} value={'M'} onChange={(e) => setPessoaFieldValue('genero', e.target.value)} /> Masculino
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="genero" checked={pessoaFormData.genero === 'F'} value={'F'} onChange={(e) => setPessoaFieldValue('genero', e.target.value)} /> Feminino
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="genero" checked={pessoaFormData.genero === 'X'} value={'X'} onChange={(e) => setPessoaFieldValue('genero', e.target.value)} /> Outro
                                </Label>
                            </FormGroup>
                        </FormGroup>
                    </div>
                    <div className="col-12">
                        <Label for="foto">Foto:</Label>
                        <Input id="foto" type="file" onChange={(e) => handleFotoChange(e)}/>
                        <FormText color="muted">
                            Foto para reconhecimento.
                        </FormText>
                    </div>
                    <hr className="my-4" />
                </div>
            </FormGroup>
            <Button color={"primary"} className={"offset-1 col-md-4"} onClick={postData}>Gravar</Button>
            <Button color={"secondary"} className={"offset-1 col-md-4"} type="reset" onClick={clearForm}>Limpar</Button>
        </Form>
    );
}