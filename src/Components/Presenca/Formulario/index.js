import React, { useState, useEffect, useCallback } from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';

export default function Formulario({presencaFormData, setPresencaFormData, setReload}) {

    const baseURL = "http://localhost:8080/presenca";

    const [pessoas, setPessoas] = useState([]);

    const token = localStorage.getItem('token')

    async function postData() {
        try {
            await fetch(`${baseURL}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(presencaFormData),
            });
            if (presencaFormData.idPresenca === undefined) {
                setPresencaFormData({})
            }
        } catch (err) {
            console.log(err);
        }
        setReload(true)
    }

    function setPresencaFieldValue(field, value) {
        let presencaTmp = {...presencaFormData}
        presencaTmp[field] = value
        setPresencaFormData(presencaTmp)
    }

    function setPresencaPessoaFieldValue(value) {
        let presencaTmp = {...presencaFormData}
        presencaTmp["pessoa"] = {
            "idPessoa": value
        }
        setPresencaFormData(presencaTmp)
    }

    const loadPessoa = useCallback(async () => {
        await fetch("http://localhost:8080/pessoa", {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then(data => {
                return data.json();
            })
            .then(data => {
                setPessoas(data);
            })
            .catch(err => {
                console.log(err);
            });
    }, [token])

    useEffect(() => {
        loadPessoa().catch(console.error);
    }, [loadPessoa]);

    return (
        <Form>
            <Input id="presenca_id" value={presencaFormData.idPresenca || ''} type='hidden' />
            <FormGroup>
                <Label for="nome">Data:</Label>
                <Input id="nome" value={presencaFormData.data || ''} onChange={(e) => setPresencaFieldValue('data', e.target.value, )} type='date' placeholder='Informe a data da presença' />
            </FormGroup>
            <FormGroup>
                <div className="form-row">
                    <div className="col-md-6">
                        <FormGroup>
                            <Label for="genero">Situação:</Label>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="situacao" checked={presencaFormData.situacao === true} value={true} onChange={(e) => setPresencaFieldValue('situacao', true)} /> Presente
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" name="situacao" checked={presencaFormData.situacao === false} value={false} onChange={(e) => setPresencaFieldValue('situacao', false)} /> Ausente
                                </Label>
                            </FormGroup>
                        </FormGroup>
                    </div>
                    <div className="col-md-6">
                        <Label for="cpf">Pessoa:</Label>
                        <Input id="cpf" type="select" value={presencaFormData.pessoa?.idPessoa ?? ''} onChange={(e) => setPresencaPessoaFieldValue(e.target.value)}>
                            {pessoas.map((pessoa, index) => (
                                <option value={pessoa.idPessoa} key={index}>{pessoa.nome}</option>
                            ))}
                        </Input>
                    </div>
                </div>
            </FormGroup>
            <Button onClick={postData}>Gravar</Button>
        </Form>
    );
}